self: super: {
  linuxPackages = super.linuxPackages.extend (self: super: {
    kernel = super.kernel.overrideAttrs (oldAttrs: rec {
      patches = oldAttrs.patches or [] ++ [ ./tascam_us_1800.patch ];
    });
  });
}
