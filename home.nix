{ config, pkgs, ... }: {
  imports = [ <home-manager/nixos> ];
  home-manager.users.reker = {
    # The home.stateVersion option does not have a default and must be set
    home.stateVersion = "24.05";
    # home.stateVersion = "24.05";

    # Here goes the rest of your home-manager config, e.g. home.packages = [ pkgs.foo ];
    # programs.vscode = {
    #   enable = true;
    #   package = pkgs.vscode.fhs;
    #   extensions = with pkgs.vscode-extensions; [
    #     dracula-theme.theme-dracula
    #     vscodevim.vim
    #     yzhang.markdown-all-in-one
    #     bbenoist.nix
    #     brettm12345.nixfmt-vscode
    #   ];
    # };
    home.packages = [ pkgs.yarn pkgs.xcape pkgs.zsh-powerlevel10k pkgs.lunarvim pkgs.neovim pkgs.anki-bin pkgs.mpv pkgs.noto-fonts-cjk-serif pkgs.noto-fonts-cjk-sans pkgs.hakuneko];

    home.keyboard.options = "ctrl:nocaps";
    # useXkbConfig = true;

    home.sessionPath = [
      "$HOME/.local/bin"
      ".git/safe/../../bin"
      # "/home/reker/.dotnet:$PATH"
      # "/home/reker/.dotnet/tools"
      "/home/reker/Programming/Dplug/tools/dplug-build"
      # "etc/nixos/DCD/bin"
      # "etc/nixos/serve-d"
    ];

    home.sessionVariables = { VST2_SDK = /home/reker/Programming/vst2_sdk; };

    programs.zsh = {
      enable = true;
      # autosuggestions.enable = true;
      syntaxHighlighting.enable = true;
      dotDir = "./zsh";
      shellAliases = {
        ll = "ls -l";
        update = "sudo nixos-rebuild switch";
        dplug-shell = "nix-shell /etc/nixos/fhsDplug.nix";
      };
      # histSize = 10000;
      # histFile = "/home/reker/.zsh_history";
      # plugins = [
      #   {
      #     name = "powerlevel10k";
      #     src = pkgs.zsh-powerlevel10k;
      #     file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      #   }
      #   {
      #     name = "powerlevel10k-config";
      #     src = lib.cleanSource ./p10k;
      #     file = "p10k.zsh";
      #   }
      # ];
      oh-my-zsh = {
        enable = true;
        plugins = [ "git" ];
        theme = "agnoster";
      };

    };
    programs.vscode = {
      enable = true;
      package = pkgs.vscode.fhs;
      mutableExtensionsDir = true;
      enableExtensionUpdateCheck = true;
      extensions = with pkgs.vscode-extensions; [
        bbenoist.nix
        vscodevim.vim 
        esbenp.prettier-vscode
        jnoortheen.nix-ide
      ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "test-adapter-converter";
          publisher = "ms-vscode";
          version = "0.1.8";
          sha256 = "sha256-ybb3Wud6MSVWEup9yNN4Y4f5lJRCL3kyrGbxB8SphDs=";
        }
        {
          name = "remote-ssh-edit";
          publisher = "ms-vscode-remote";
          version = "0.47.2";
          sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
        }
        {
          name = "code-d";
          publisher = "webfreak";
          version = "0.23.2";
          sha256 = "sha256-v/Dck4gE9kRkfIWPAkUmPqewyTVVKrBgAjpNuCROClE=";
        }
        {
          name = "debug";
          publisher = "webfreak";
          version = "0.26.1";
          sha256 = "sha256-lLLa8SN+Sf9Tbi7HeWYWa2KhPQFJyQWrf9l3EUljwYo=";
        }
        {
          name = "vscode-test-explorer";
          publisher = "hbenl";
          version = "2.21.1";
          sha256 = "sha256-fHyePd8fYPt7zPHBGiVmd8fRx+IM3/cSBCyiI/C0VAg=";
        }
        # {
        #   name = "cpptools";
        #   publisher = "ms-vscode";
        #   version = "1.19.0";
        #   sha256 = "sha256-J3/ehLFZivyF7uy5sON7iHwyAuLPLAzGhh1SfOenlt4=";
        # }
      ];

    };


    programs.git = {
      enable = true;
      userName = "Ethan P. Reker";
      userEmail = "ethan@cutthroughrecordings.com";
    };
  };

}
