# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
{
# let
#   unstable = import (builtins.fetchTarball "channel:nixos-unstable") {
#     config = config.nixpkgs.config;
#   };
# in {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./home.nix
    ./libvirt.nix
    ./steam.nix
    ./gpu.nix
    ./audio.nix
    # ./unstable.nix
  ];
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  # permittedInsecurePackages = [ "electron-25.9.0" ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 8;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  i18n.supportedLocales = [
    "en_US.UTF-8/UTF-8"
    "ja_JP.UTF-8/UTF-8"
  ];

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  # services.desktopManager.plasma6.enable = true;

  #services.xserver.enable = true;
  #services.xserver.displayManager.gdm.enable = true;
  #services.xserver.desktopManager.gnome.enable = true;

  # qt = {
  #   enable = true;
  #   platformTheme = "gnome";
  #   style = "adwaita-dark";
  # };

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  users.users.reker = {
    isNormalUser = true;
    home = "/home/reker";
    description = "Ethan Reker";
    extraGroups = [ "wheel" "networkmanager" "libvirtd" "audio"];
    packages = [ pkgs.firefox pkgs.kate pkgs.nixfmt ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Allow Electron version for obsidian
  nixpkgs.config.permittedInsecurePackages = [ "electron-25.9.0" ];

  # services.plex = {
  #   enable = true;
  #   openFirewall = true;
  # };
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
    vim
    pkgs.ldc
    pkgs.dmd
    pkgs.dub
    pkgs.xorg.libX11
    pkgs.gdb
    pkgs.lldb
    pkgs.git
    pkgs.freerdp
    pkgs.remmina
    pkgs.reaper
    pkgs.ardour
    # pkgs.bitwig-studio
    pkgs.carla
    pkgs.lsp-plugins
    pkgs.lv2
    pkgs.obsidian
    pkgs.nextcloud-client
    pkgs.discord
    pkgs.deluge
    pkgs.filezilla
    pkgs.vlc
    # pkgs.wpsoffice
    pkgs.darktable
    pkgs.faust

    # pkgs.protonvpn-gui
    # pkgs.protonvpn-cli
    #unstable.protonvpn-gui

    pkgs.yt-dlp
    pkgs.ledger-live-desktop
    pkgs.ledger-udev-rules

    pkgs.swtpm
    pkgs.gparted
    pkgs.spotify
    pkgs.chromium
    pkgs.virtio-win
    pkgs.virglrenderer
    pkgs.zoom-us
    pkgs.chezmoi
    pkgs.bleachbit
    pkgs.obs-studio

    pkgs.mediawriter
    pkgs.ventoy-full
    pkgs.inkscape-with-extensions
    pkgs.libsForQt5.kdenlive
    pkgs.glaxnimate
    pkgs.ffmpeg_5-full
    pkgs.teams-for-linux
    pkgs.libsForQt5.plasma-nm
    pkgs.openconnect
    pkgs.python3
    pkgs.python311Packages.pip
    pkgs.poetry
    pkgs.gnumake
    pkgs.ripgrep
    pkgs.gcc
    pkgs.fd
    pkgs.lazygit
    #pkgs.qt6.qtwebengine
    #pkgs.openconnect
    #pkgs.oath-toolkit
    # pkgs.libsForQt5.qt5.qtwebengine

    # (vscode-with-extensions.override {
    #   vscodeExtensions = with vscode-extensions;
    #     [
    #       bbenoist.nix
    #       ms-python.python
    #       dracula-theme.theme-dracula
    #       vscodevim.vim
    #       yzhang.markdown-all-in-one
    #       bbenoist.nix
    #       brettm12345.nixfmt-vscode

    #     ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
    #       {
    #         name = "test-adapter-converter";
    #         publisher = "ms-vscode";
    #         version = "0.1.8";
    #         sha256 = "sha256-ybb3Wud6MSVWEup9yNN4Y4f5lJRCL3kyrGbxB8SphDs=";
    #       }
    #       {
    #         name = "remote-ssh-edit";
    #         publisher = "ms-vscode-remote";
    #         version = "0.47.2";
    #         sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
    #       }
    #       {
    #         name = "code-d";
    #         publisher = "webfreak";
    #         version = "0.23.2";
    #         sha256 = "sha256-v/Dck4gE9kRkfIWPAkUmPqewyTVVKrBgAjpNuCROClE=";
    #       }
    #       {
    #         name = "debug";
    #         publisher = "webfreak";
    #         version = "0.26.1";
    #         sha256 = "sha256-lLLa8SN+Sf9Tbi7HeWYWa2KhPQFJyQWrf9l3EUljwYo=";
    #       }
    #       {
    #         name = "vscode-test-explorer";
    #         publisher = "hbenl";
    #         version = "2.21.1";
    #         sha256 = "sha256-fHyePd8fYPt7zPHBGiVmd8fRx+IM3/cSBCyiI/C0VAg=";
    #       }
    #       {
    #         name = "cpptools";
    #         publisher = "ms-vscode";
    #         version = "1.19.0";
    #         sha256 =
    #           "sha256-J3/ehLFZivyF7uy5sON7iHwyAuLPLAzGhh1SfOenlt4=\n          ";
    #       }
    #     ];
    # })
  ];

  # nixpkgs.overlays = [
  #   (import "${builtins.fetchTarball https://github.com/vlaci/openconnect-sso/archive/master.tar.gz}/overlay.nix")
  # ];
  nixpkgs.overlays = [
    (import ./patches)
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.enableIPv6 = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

}
