{ config, pkgs, lib, ... }: {
  boot.kernelModules = [ "kvm-amd" ];

  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu.ovmf.enable = true;
  virtualisation.libvirtd.qemu.swtpm.enable = true;
  programs.virt-manager.enable = true;

  boot.extraModprobeConfig = ''
    options kvm_intel kvm_amd nested=1
    options kvm_intel emulate_invalid_guest_state=0
    options kvm ignore_msrs=1
  '';

  virtualisation.libvirtd.extraConfig = ''
    unix_sock_group = "libvirtd"
    unix_sock_rw_perms = "0770"
    log_filters="1:qemu"
    log_outputs="1:file:/var/log/libvirt/libvirtd.log"
  '';
}
